#!/bin/bash

# @brief Launches the user into the developer container

docker run --rm -w /topfood --net=host\
    --env AWS_ACCESS_KEY_ID=$TOPFOOD_AWS_ACCESS_KEY_ID\
    --env AWS_SECRET_ACCESS_KEY=$TOPFOOD_AWS_SECRET_ACCESS_KEY\
    --env AWS_DEFAULT_REGION=us-east-2\
    -itv $PWD:/topfood topfood/api:latest-dev