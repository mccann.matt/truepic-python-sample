#!/usr/bin/env python3

"""
@brief Builds the docker image
"""

import os
import subprocess
import sys


def print_usage():
    ''' Prints out the command-line interface to aid the user with usage. '''
    print("Usage: %s (version)" % sys.argv[0])
    sys.exit(-1)

# Extract the version tag from the command-line arguments
version_tag = 'latest' if len(sys.argv) < 2 else sys.argv[1]

# Copy the Dockerfile into the root directory
os.system('cp docker/Dockerfile .')

# Collect the user and group details
group_id = subprocess.check_output(['id', '-rg']).decode('utf-8').rstrip()
group_name = subprocess.check_output(['id', '-rgn']).decode('utf-8').rstrip()
user_id = subprocess.check_output(['id', '-ru']).decode('utf-8').rstrip()
user_name = subprocess.check_output(['id', '-run']).decode('utf-8').rstrip()

try:
    command = 'docker build -t topfood/api:%s-dev --build-arg GROUP_ID=%s ' \
        '--build-arg GROUP_NAME=%s --build-arg USER_ID=%s --build-arg USER_NAME=%s .'
    command = command % (version_tag, group_id, group_name, user_id, user_name)

    # Build the Docker image
    print(command)
    sys.exit(os.system(command))

finally:
    # Clean up the generated Dockerfile
    os.system('rm Dockerfile')
