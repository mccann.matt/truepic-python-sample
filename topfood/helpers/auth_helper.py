"""
@brief Provides authentication policy decorators
"""

import flask_login

from functools import wraps

from topfood.helpers.response_helper import build_access_denied_response


def admin_only(func):
    @flask_login.login_required
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if flask_login.current_user.is_admin is False:
            return build_access_denied_response()
        return func(*args, **kwargs)
    return decorated_view
