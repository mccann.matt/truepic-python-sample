import flask

from topfood.helpers.assert_helper import assert_not_empty_string


def build_access_denied_response(message=None):
    if message:
        response = flask.jsonify({'message': message})
        response.status_code = 403

        return response
    else:
        response = flask.jsonify()
        response.status_code = 403

        return response


def build_bad_request_response(message, payload=None):
    """
    Payload is any additional description details e.g. a list of missing fields
    """
    assert_not_empty_string(message)

    body = {'message': message}
    if payload is not None:
        body['payload'] = payload

    response = flask.jsonify(body)
    response.status_code = 400

    return response


def build_conflict_response(message):
    assert_not_empty_string(message)

    response = flask.jsonify({'message': message})
    response.status_code = 409

    return response


def build_ok_response(payload=None):
    # If the payload is a Record that needs to be converted into a json friendly representation
    if hasattr(payload, 'to_json_friendly'):
        payload = payload.to_json_friendly()

    return flask.jsonify(payload)
