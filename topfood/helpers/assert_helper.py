import re


def assert_not_empty_string(argument):
    if argument is None or argument == '':
        raise ValueError('must not be empty')


def assert_max_length_string(max_length, argument):
    if len(argument) > max_length:
        raise ValueError('length must be <= %s, got %s' % (max_length, len(argument)))


def assert_max_value(max_value, argument):
    if argument > max_value:
        raise ValueError('must be <= %s, got %s' % (max_value, argument))


def assert_min_length_string(min_length, argument):
    if len(argument) < min_length:
        raise ValueError('length must be >= %s, got %s' % (min_length, argument))


def assert_min_value(min_value, argument):
    if argument < min_value:
        raise ValueError('must be >= %s, got %s' % (min_value, argument))


def assert_valid_email(email):
    if not re.match("[^@]+@[^@]+\.[^@]+", email):
        raise ValueError('format is invalid')
