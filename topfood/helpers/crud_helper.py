import flask

from topfood.api.api_root import db
from topfood.helpers.response_helper import build_bad_request_response, build_ok_response


def create_record(record_class, request_data):
    record = record_class()

    did_error_occur, error_response = record.apply_create_request_fields(request_data)
    if did_error_occur:
        return error_response

    # Build the response before commit to avoid staleness re-read
    response = build_ok_response(record)

    db.session.add(record)
    db.session.commit()

    return response


def delete_record(record_class, record_id):
    record = record_class.query.get(record_id)
    if record is None:
        return build_bad_request_response('no matching record')

    db.session.delete(record)
    db.session.commit()

    return build_ok_response()


def list_records(record_class, order_by=None):
    query = db.session.query(record_class)

    if order_by is not None:
        query = query.order_by(order_by)

    records = query.all()

    return flask.jsonify([record.to_json_friendly() for record in records])


def update_record(record_class, record_id, request_data):
    record = record_class.query.get(record_id)
    if record is None:
        return build_bad_request_response('no matching record')

    did_apply_fail, error_response = record.apply_update_request_fields(request_data)
    if did_apply_fail:
        return error_response

    db.session.commit()

    return build_ok_response()
