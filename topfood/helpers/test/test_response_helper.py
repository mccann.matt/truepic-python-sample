import topfood.helpers.response_helper as helper

from topfood.db.models.account import Account
from topfood.test.test_case import TestCase


class TestResponseHelpers(TestCase):
    def test_build_bad_request_response__empty_message(self):
        try:
            helper.build_bad_request_response('')
            assert False, "Should have thrown"
        except ValueError:
            pass  # Threw as expected

    def test_build_bad_request_response__no_payload(self):
        response = helper.build_bad_request_response('missing fields')

        assert response.get_json() == {'message': 'missing fields'}
        assert response.status_code == 400

    def test_build_bad_request_response__with_payload(self):
        response = helper.build_bad_request_response('missing fields', ['email', 'name'])

        assert response.get_json() == {
            'message': 'missing fields',
            'payload': ['email', 'name'],
        }
        assert response.status_code == 400

    def test_build_conflict_response__empty_message(self):
        try:
            helper.build_conflict_response('')
            assert False, 'Should have thrown'
        except ValueError:
            pass  # Threw as expected

    def test_build_conflict_response(self):
        response = helper.build_conflict_response('email already exists')

        assert response.get_json() == {'message': 'email already exists'}
        assert response.status_code == 409

    def test_build_ok_response__no_payload(self):
        response = helper.build_ok_response()

        assert response.get_json() is None
        assert response.status_code == 200

    def test_build_ok_response__with_plain_payload(self):
        response = helper.build_ok_response([3, 4, 5])

        assert response.get_json() == [3, 4, 5]
        assert response.status_code == 200

    def test_build_ok_response__with_record_payload(self):
        record = Account()
        record.name = 'Bob Villa'
        record.email = 'bob@bob.com'

        response = helper.build_ok_response(record)

        assert response.get_json() == {
            'email': 'bob@bob.com',
            'id': record.id,
            'is_admin': False,
            'name': 'Bob Villa'
        }, response.get_json()
        assert response.status_code == 200

