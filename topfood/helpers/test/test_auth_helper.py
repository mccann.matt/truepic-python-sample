"""
@brief Unit tests for the auth helper decorators AND general auth testing helper functions
"""

import json

from topfood.api.api_root import db
from topfood.db.models.account import Account


def create_account_but_dont_login(api):
    """
    Creates a test account but does not log into it.

    @param api Test client for the api
    @returns Id of the test account
    """
    # Create the test profile
    dev_profile_data = {
        'email': 'mccann.matt@gmail.com',
        'name': 'Matt McCann',
        'password': 'password'
    }
    response = api.post('/account', data=json.dumps(dev_profile_data))
    decoded = json.loads(response.get_data(as_text=True))

    return decoded['id']


def create_non_admin_account_and_login(api):
    """
    Creates a test account and logs into it.

    @param api Test client for the api
    @returns Id of the test account
    """
    # Create the test profile
    dev_profile_data = {
        'email': 'mccann.matt@gmail.com',
        'name': 'Bob Villa',
        'password': 'password'
    }
    response = api.post('/account', data=json.dumps(dev_profile_data))
    decoded = json.loads(response.get_data(as_text=True))

    # Log in to the profile
    login_data = {
        'email': 'mccann.matt@gmail.com',
        'password': 'password'
    }
    api.post('/auth/login', data=json.dumps(login_data))

    return decoded['id']


def create_admin_and_login(api):
    """
    Creates an admin account and logs into it.

    @param api Test client for the api
    @returns Id of the admin account
    """
    # Create the admin account
    admin = Account()
    admin.email = 'mccann.matt@gmail.com'
    admin.is_admin = True
    admin.name = 'Bob Villa'
    admin.password = 'password'
    db.session.add(admin)
    db.session.commit()

    # Log in to the profile
    login_data = {
        'email': 'mccann.matt@gmail.com',
        'password': 'password',
    }
    response = api.post('/auth/login', data=json.dumps(login_data), content_type='application/json')
    decoded = json.loads(response.get_data(as_text=True))

    return decoded['id']


def create_account_but_login_to_another(api):
    """
    Creates a test account but logs into a different test account.

    @param api Test client for the api
    @returns Id of the test account NOT logged into
    """
    create_non_admin_account_and_login(api)

    dev_profile_data = {
        'email': 'mccann.matt2@gmail.com',
        'name': 'Matt McCann',
        'password': 'password'
    }
    response = api.post('/account', data=json.dumps(dev_profile_data), content_type='application/json')
    decoded = json.loads(response.get_data(as_text=True))

    return decoded['id']
