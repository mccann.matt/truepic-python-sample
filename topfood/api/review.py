import flask
import flask_login

from topfood.api.api_root import api, db
from topfood.db.models.restaurant import Restaurant
from topfood.db.models.review import Review
from topfood.helpers.auth_helper import admin_only
from topfood.helpers.response_helper import build_bad_request_response, build_conflict_response, build_ok_response


@api.route('/review', methods=['POST'])
@flask_login.login_required
def review_create():
    request_data = flask.request.json

    review = Review()
    review.author_id = flask_login.current_user.id

    did_error_occur, error_response = review.apply_create_request_fields(request_data)
    if did_error_occur:
        return error_response

    # If the user already reviewed this restaurant
    duplicate_reviews = Review.query.filter_by(author_id=flask_login.current_user.id)\
                                    .filter_by(restaurant_id=request_data['restaurant_id']).all()
    if len(duplicate_reviews) > 0:
        return build_conflict_response('user already reviewed restaurant')

    db.session.add(review)

    # Update the summary details of the restaurant that's been reviewed
    restaurant = Restaurant.query.get(review.restaurant_id)
    if restaurant is None:
        return build_bad_request_response('malformed fields', {'restaurant_id': 'no matching record'})
    restaurant.maintain_summary()

    # Build the response before commit to avoid staleness re-read
    response = build_ok_response(review)

    db.session.commit()

    return response


@api.route('/review/<review_id>', methods=['DELETE'])
@admin_only
def review_delete(review_id):
    review = Review.query.get(review_id)
    if review is None:
        return build_bad_request_response('no matching record')

    db.session.delete(review)

    # Update the summary details of the restaurant that's been reviewed
    restaurant = Restaurant.query.get(review.restaurant_id)
    restaurant.maintain_summary()

    db.session.commit()

    return build_ok_response()


@api.route('/review/<review_id>', methods=['PUT'])
@admin_only
def review_update(review_id):
    request_data = flask.request.json

    review = Review.query.get(review_id)
    if review is None:
        return build_bad_request_response('no matching record')

    did_apply_fail, error_response = review.apply_update_request_fields(request_data)
    if did_apply_fail:
        return error_response

    # Update the summary details of the restaurant that's been reviewed
    restaurant = Restaurant.query.get(review.restaurant_id)
    restaurant.maintain_summary()

    db.session.commit()

    return build_ok_response()
