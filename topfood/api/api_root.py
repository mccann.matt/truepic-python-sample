import flask
import os

from flask import Flask
from flask_cors import CORS
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from topfood.db import ProductionConfig, TestConfig

if 'TARGET' not in os.environ:
    os.environ['TARGET'] = 'test'

# Initialize the Flask module
api = Flask(__name__)
api.secret_key = '\xac\xf7q{,\x82D\xb5\xf8~ci\x11=b\xe8Nb\x15\x94)Kh\xfb'

CORS(
    api,
    resources={r"/*": {"origins": ["https://topfood.mattmccann.xyz", "http://localhost"]}},
    supports_credentials=True
)

# Configure the database settings
if os.environ['TARGET'] == 'production':
    api.config.from_object(ProductionConfig)
else:
    api.config.from_object(TestConfig)
db = SQLAlchemy(api)
migrate = Migrate(api, db)

# Enable and initialize the Flask login module
lm = LoginManager()
lm.init_app(api)

# Import our APIs
import topfood.api.account # noqa
import topfood.api.auth # noqa
import topfood.api.restaurant # noqa
import topfood.api.review # noqa


@api.route('/')
def hello_world():
    """ Simple endpoint to check if the api is responding """
    return flask.jsonify({'message': 'hello'})
