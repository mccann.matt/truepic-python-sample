import flask
import flask_login

from topfood.api.api_root import api, lm
from topfood.db.models.account import Account
from topfood.helpers.response_helper import build_access_denied_response, build_ok_response


@lm.user_loader
def load(account_id):
    """ Helper function for the Flask login manager to fetch the account. """
    return Account.query.get(account_id)


@api.route('/auth/login', methods=['POST'])
def login():
    failure_message = 'unrecognized email/password combo'
    request = flask.request.json

    account = Account.query.filter_by(email=request['email']).all()

    if len(account) == 0:
        return build_access_denied_response(failure_message)
    account = account[0]

    if account.is_matching_password(request['password']):
        flask_login.login_user(account)

        return build_ok_response(account)
    else:
        return build_access_denied_response(failure_message)


@api.route('/auth/logout', methods=['POST'])
@flask_login.login_required
def logout():
    flask_login.logout_user()

    return build_ok_response()
