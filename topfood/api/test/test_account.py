import json

from topfood.api.api_root import db
from topfood.helpers.test.test_auth_helper import create_non_admin_account_and_login, create_admin_and_login
from topfood.db.models.account import Account
from topfood.db.models.review import Review
from topfood.db.models.restaurant import Restaurant
from topfood.test.test_case import TestCase


class TestAccount(TestCase):
    def test_create__email_already_registered(self):
        request = {
            'name': 'Bob Villa',
            'email': 'bob@bob.com',
            'password': 'password'
        }

        self.create_account()  # Create a conflicting user account

        response = self.client.post('/account', data=json.dumps(request))

        assert response.status_code == 409, response
        assert response.get_json() == {'message': 'duplicate email'}

    def test_create__malformed_field_values(self):
        request = {
            'name': 'A'*100,
            'email': 'bob.villa',
            'password': 'password'
        }

        response = self.client.post('/account', data=json.dumps(request))

        assert response.status_code == 400, response.status_code
        assert response.get_json() == {
            'message': 'malformed fields',
            'payload': {
                'email': 'format is invalid',
                'name': 'length must be <= 64, got 100',
            },
        }, response.get_json()

    def test_create__missing_fields(self):
        request = {
            'email': 'bob.villa2@bobvilla.com',
        }

        response = self.client.post('/account', data=json.dumps(request))

        assert response.status_code == 400, response.status_code
        assert response.get_json() == {
            'message': 'missing required fields',
            'payload': ['name', 'password'],
        }, response.get_json()

    def test_create__good(self):
        """ Tests registering an account with valid information """
        request = {
            'name': 'Bobby Villa',
            'email': 'bob.villa2@bobvilla.com',
            'password': 'password'
        }

        response = self.client.post('/account', data=json.dumps(request))

        # Check that the account record was created
        account = Account.query.filter_by(email='bob.villa2@bobvilla.com').one()

        assert response.status_code == 200, response
        assert response.get_json() == {
            'id': account.id,
            'email': request['email'],
            'is_admin': False,
            'name': request['name'],
        }

    def test_delete__not_logged_in(self):
        account = self.create_account()

        response = self.client.delete('/account/%s' % account.id)

        assert response.status_code == 401, response

    def test_delete__not_admin(self):
        account = self.create_account()

        create_non_admin_account_and_login(self.client)

        response = self.client.delete('/account/%s' % account.id)

        assert response.status_code == 403, response

    def test_delete__does_not_exist(self):
        create_admin_and_login(self.client)

        response = self.client.delete('/account/does_not_exist')

        assert response.status_code == 400, response

        # Check that response is descriptive
        body = json.loads(response.data)
        assert body['message'] == 'no matching record', body

    def test_delete__good(self):
        # Setup the test data
        account = self.create_account()
        restaurant = self.create_restaurant()
        self.create_review(account, restaurant)

        # Generate a summary that includes the account's review
        restaurant.maintain_summary()
        db.session.commit()

        create_admin_and_login(self.client)

        response = self.client.delete('/account/%s' % account.id)

        assert response.status_code == 200, response

        # Check that the account was deleted from the database
        accounts = Account.query.filter_by(email='bob.villa2@bobvilla.com').all()
        assert len(accounts) == 0

        # Check that the restaurant causes a cascade delete of the restaurant's reviews
        reviews = Review.query.filter_by(author_id=account.id).all()
        assert len(reviews) == 0

        # Check that the restaurants impacted by the user's reviews being deleted had their
        # summary details recalculated
        restaurant = Restaurant.query.get(restaurant.id)
        assert restaurant.num_reviews == 0
        assert restaurant.avg_rating == 0
        assert restaurant.best_review_id is None
        assert restaurant.worst_review_id is None

    def test_list__not_logged_in(self):
        response = self.client.get('/account')

        assert response.status_code == 401, response

    def test_list__not_admin(self):
        create_non_admin_account_and_login(self.client)

        response = self.client.get('/account')

        assert response.status_code == 403, response

    def test_list__good(self):
        self.create_account('2@bob.com')
        self.create_account('3@bob.com')
        self.create_account('1@bob.com')

        create_admin_and_login(self.client)

        response = self.client.get('/account')

        assert response.status_code == 200, response
        body = response.get_json()
        assert len(body) == 4, body
        assert body[0]['email'] == '1@bob.com', body[0]
        assert body[1]['email'] == '2@bob.com', body[1]
        assert body[2]['email'] == '3@bob.com', body[2]
        assert body[3]['email'] == 'mccann.matt@gmail.com', body[3]  # Created by create_admin_and_login

    def test_update__not_logged_in(self):
        account = self.create_account()

        changes = {'name': 'Bob Barker'}
        response = self.client.put('/account/%s' % account.id, data=json.dumps(changes))

        assert response.status_code == 401, response

    def test_update__not_admin(self):
        account = self.create_account()

        create_non_admin_account_and_login(self.client)

        changes = {'name': 'Bob Barker'}
        response = self.client.put('/account/%s' % account.id, data=json.dumps(changes))

        assert response.status_code == 403, response

    def test_update__does_not_exist(self):
        create_admin_and_login(self.client)

        changes = {'name': 'Bob Barker'}
        response = self.client.put('/account/does_not_exist', data=json.dumps(changes))

        assert response.status_code == 400, response
        assert response.get_json() == {'message': 'no matching record'}

    def test_update__email_already_registered(self):
        create_admin_and_login(self.client)

        # Create a conflicting user account we will conflict with on update
        self.create_account('bob@thisoldehome.org')

        # Create the account we are going to update
        account = self.create_account('bob@bob.com')

        request = {'email': 'bob@thisoldehome.org'}
        response = self.client.put('/account/%s' % account.id, data=json.dumps(request))

        assert response.status_code == 409
        assert response.get_json() == {'message': 'duplicate email'}

    def test_update__good(self):
        account = self.create_account()

        create_admin_and_login(self.client)

        changes = {
            'name': 'Bob Barker',
            'email': 'bob@barker.com',
            'is_admin': True,
        }
        response = self.client.put('/account/%s' % account.id, data=json.dumps(changes))

        assert response.status_code == 200, response

        # Check that the account was updated in the database
        account = Account.query.get(account.id)
        assert account.email == 'bob@barker.com', account
        assert account.name == 'Bob Barker', account
        assert account.is_admin is True, account
