import json

import topfood.api.auth

from topfood.api.api_root import db
from topfood.db.models.account import Account
from topfood.test.test_case import TestCase


class TestAuth(TestCase):
    def setUp(self):
        super(TestAuth, self).setUp()

        # Set up an account object to work with
        self.account = Account()
        self.account.name = 'Bob Villa'
        self.account.email = 'bob@bob.com'
        self.account.is_admin = True
        self.account.password = 'password'
        db.session.add(self.account)
        db.session.commit()

    def test_load__account_missing(self):
        assert topfood.api.auth.load('id') is None

    def test_load__account_exists(self):
        # Check that the account is returned
        assert topfood.api.auth.load(self.account.id).id == self.account.id

    def test_login__account_does_not_exist(self):
        request = {
            'email': 'bob.villa@bobvilla.com',
            'password': 'password'
        }

        response = self.client.post('/auth/login', data=json.dumps(request))

        assert response.status_code == 403, response.status_code
        assert response.get_json() == {'message': 'unrecognized email/password combo'}

    def test_login__account_non_matching_password(self):
        request = {
            'email': 'bob@bob.com',
            'password': 'qwerty'
        }

        # Request the login
        response = self.client.post('/auth/login', data=json.dumps(request))

        # Check the response
        assert response.status_code == 403, response.status_code
        assert response.get_json() == {'message': 'unrecognized email/password combo'}

    def test_login__good(self):
        request = {
            'email': 'bob@bob.com',
            'password': 'password',
        }

        response = self.client.post('/auth/login', data=json.dumps(request))

        assert response.status_code == 200, response.status_code

        # Try to access a login-required endpoint
        response = self.client.put('/account/%s' % self.account.id, data=json.dumps({}))
        assert response.status_code == 200, response.status_code

    def test_logout(self):
        request = {
            'email': 'bob@bob.com',
            'password': 'password',
        }

        self.client.post('/auth/login', data=json.dumps(request))

        # Try to access a login-required endpoint
        response = self.client.put('/account/%s' % self.account.id, data=json.dumps({}))
        assert response.status_code == 200, response.status_code

        response = self.client.post('/auth/logout')
        assert response.status_code == 200, response.get_json()

        # Try to access a login-required endpoint
        response = self.client.put('/account/%s' % self.account.id, data=json.dumps({}))
        assert response.status_code == 401, response.status_code
