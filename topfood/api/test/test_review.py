import json

from datetime import date

from topfood.api.api_root import db
from topfood.helpers.test.test_auth_helper import create_non_admin_account_and_login, create_admin_and_login
from topfood.db.models.account import Account
from topfood.db.models.restaurant import Restaurant
from topfood.db.models.review import Review
from topfood.test.test_case import TestCase


def create_review(author_id, restaurant_id, rating=3.0):
    review = Review()
    review.author_id = author_id
    review.restaurant_id = restaurant_id
    review.rating = rating
    review.visit_date = date.today()

    db.session.add(review)
    db.session.commit()

    return review.id


class TestReview(TestCase):
    def setUp(self):
        super(TestReview, self).setUp()

        self.account = self.create_account()
        self.restaurant = self.create_restaurant()
        self.review = self.create_review(self.account, self.restaurant, rating=3.0)

    def test_create__not_logged_in(self):
        response = self.client.post('/review')

        assert response.status_code == 401, response

    def test_create__duplicate_review(self):
        create_non_admin_account_and_login(self.client)

        # Create the first review
        request = {
            'comment': 'lorem ipsum',
            'rating': 5.0,
            'restaurant_id': self.restaurant.id,
            'visit_date': '2018-09-19',
        }
        self.client.post('/review', data=json.dumps(request))

        # Attempt to create a duplicate review
        response = self.client.post('/review', data=json.dumps(request))

        assert response.status_code == 409
        assert response.get_json() == {'message': 'user already reviewed restaurant'}

    def test_create__missing_fields(self):
        create_non_admin_account_and_login(self.client)

        request = {
            'rating': 4,
        }
        response = self.client.post('/review', data=json.dumps(request))

        assert response.status_code == 400, response.status_code
        assert response.get_json() == {
            'message': 'missing required fields',
            'payload': ['restaurant_id', 'visit_date'],
        }

    def test_create__malformed_fields(self):
        create_non_admin_account_and_login(self.client)

        request = {
            'rating': 9.0,
            'restaurant_id': self.restaurant.id,
            'visit_date': '2018/09/19',
        }
        response = self.client.post('/review', data=json.dumps(request))

        assert response.status_code == 400, response.status_code
        assert response.get_json() == {
            'message': 'malformed fields',
            'payload': {
                'rating': 'must be <= 5, got 9.0',
                'visit_date': 'format must be %Y-%m-%d, got 2018/09/19',
            },
        }, response.get_json()

    def test_create__restaurant_doesnt_exist(self):
        create_non_admin_account_and_login(self.client)

        request = {
            'rating': 5.0,
            'restaurant_id': 'does not exist',
            'visit_date': '2018-09-19',
        }
        response = self.client.post('/review', data=json.dumps(request))

        assert response.status_code == 400, response.status_code
        assert response.get_json() == {
            'message': 'malformed fields',
            'payload': {
                'restaurant_id': 'no matching record',
            },
        }, response.get_json()

    def test_create__good(self):
        account_id = create_non_admin_account_and_login(self.client)

        request = {
            'comment': 'lorem ipsum',
            'rating': 5.0,
            'restaurant_id': self.restaurant.id,
            'visit_date': '2018-09-19',
        }
        response = self.client.post('/review', data=json.dumps(request))

        assert response.status_code == 200, response.get_json()

        body = response.get_json()
        assert body['comment'] == request['comment'], body
        assert body['rating'] == request['rating'], body
        assert body['visit_date'] == request['visit_date'], body

        review = Review.query.get(body['id'])
        assert review.author_id == account_id
        assert review.comment == request['comment']
        assert review.rating == request['rating']
        assert review.restaurant_id == self.restaurant.id
        assert review.visit_date == request['visit_date']

        # Check that the restaurant summary state was updated
        restaurant = Restaurant.query.get(self.restaurant.id)
        assert restaurant.num_reviews == 2
        assert restaurant.avg_rating == 4.0
        assert restaurant.best_review_id == review.id
        assert restaurant.worst_review_id == self.review.id

    def test_delete__not_logged_in(self):
        response = self.client.delete('/review/%s' % self.review.id)

        assert response.status_code == 401, response

    def test_delete__not_admin(self):
        create_non_admin_account_and_login(self.client)

        response = self.client.delete('/review/%s' % self.review.id)

        assert response.status_code == 403, response

    def test_delete__record_doesnt_exist(self):
        create_admin_and_login(self.client)

        response = self.client.delete('/review/does_not_exist')

        assert response.status_code == 400, response

    def test_delete__good(self):
        create_admin_and_login(self.client)

        response = self.client.delete('/review/%s' % self.review.id)

        assert response.status_code == 200, response

        # Check that the record was deleted from the database
        assert Restaurant.query.get(self.review.id) is None

        # Check that the restaurant's summary data was updated
        restaurant = Restaurant.query.get(self.restaurant.id)
        assert restaurant.num_reviews == 0
        assert restaurant.avg_rating == 0
        assert restaurant.best_review_id is None
        assert restaurant.worst_review_id is None

    def test_update__not_logged_in(self):
        changes = {
            'comment': 'lorem ipsum'
        }
        response = self.client.put('/review/%s' % self.review.id, data=json.dumps(changes))

        assert response.status_code == 401, response

    def test_update__not_admin(self):
        create_non_admin_account_and_login(self.client)

        changes = {
            'comment': 'lorem ipsum'
        }
        response = self.client.put('/review/%s' % self.review.id, data=json.dumps(changes))

        assert response.status_code == 403, response

    def test_update__record_doesnt_exist(self):
        create_admin_and_login(self.client)

        changes = {
            'comment': 'lorem ipsum'
        }
        response = self.client.put('/review/does_not_exist', data=json.dumps(changes))

        assert response.status_code == 400, response
        assert response.get_json() == {'message': 'no matching record'}, response.get_json()

    def test_update__malformed_fields(self):
        create_admin_and_login(self.client)

        changes = {
            'rating': 9.0,
        }
        response = self.client.put('/review/%s' % self.review.id, data=json.dumps(changes))

        assert response.status_code == 400, response
        assert response.get_json() == {
            'message': 'malformed fields',
            'payload': {
                'rating': 'must be <= 5, got 9.0',
            },
        }, response.get_json()

    def test_update__good__rating_not_changed(self):
        create_admin_and_login(self.client)

        changes = {
            'comment': 'lorem ipsum',
        }
        response = self.client.put('/review/%s' % self.review.id, data=json.dumps(changes))

        assert response.status_code == 200, response

        review = Review.query.get(self.review.id)
        assert review.comment == 'lorem ipsum'

    def test_update__good__rating_changed(self):
        create_admin_and_login(self.client)

        changes = {
            'rating': 5.0,
        }
        response = self.client.put('/review/%s' % self.review.id, data=json.dumps(changes))

        assert response.status_code == 200, response

        review = Review.query.get(self.review.id)
        assert review.rating == 5.0

        # Check that the restaurant summary details were updated
        restaurant = Restaurant.query.get(self.restaurant.id)
        assert restaurant.avg_rating == 5.0
