import json

from topfood.api.api_root import db
from topfood.helpers.test.test_auth_helper import create_non_admin_account_and_login, create_admin_and_login
from topfood.db.models.account import Account
from topfood.db.models.restaurant import Restaurant
from topfood.db.models.review import Review
from topfood.test.test_case import TestCase


def create_restaurant(name='KFC', avg_rating=3.0):
    record = Restaurant()
    record.name = name
    record.image = 'image.jpg'
    record._avg_rating = avg_rating

    db.session.add(record)
    db.session.commit()

    return record.id


def create_review(author_id, restaurant_id, rating=3.0):
    review = Review()
    review.author_id = author_id
    review.restaurant_id = restaurant_id
    review.rating = rating
    review.visit_date = '2018-12-12'

    db.session.add(review)

    return review


class TestRestaurant(TestCase):
    def setUp(self):
        super(TestRestaurant, self).setUp()

        self.author = Account()
        self.author.name = 'Bob Villa'
        self.author.email = 'bob@bob.com'
        self.author.password = 'password'

        db.session.add(self.author)
        db.session.commit()

    def test_create__not_logged_in(self):
        response = self.client.post('/restaurant')

        assert response.status_code == 401, response

    def test_create__not_admin(self):
        create_non_admin_account_and_login(self.client)

        response = self.client.post('/restaurant')

        assert response.status_code == 403, response

    def test_create__malformed_field_value(self):
        create_admin_and_login(self.client)

        request = {
            'name': 'A'*100,
            'image': 'image.jpg',
        }
        response = self.client.post('/restaurant', data=json.dumps(request))

        assert response.status_code == 400, response.status_code
        assert response.get_json() == {
            'message': 'malformed fields',
            'payload': {
                'name': 'length must be <= 64, got 100',
            },
        }

    def test_create__missing_field(self):
        create_admin_and_login(self.client)

        request = {
            'image': 'image.jpg',
        }
        response = self.client.post('/restaurant', data=json.dumps(request))

        assert response.status_code == 400, response.status_code
        assert response.get_json() == {
            'message': 'missing required fields',
            'payload': ['name'],
        }

    def test_create__good(self):
        create_admin_and_login(self.client)

        request = {
            'name': 'KFC',
            'image': 'image.jpg',
        }
        response = self.client.post('/restaurant', data=json.dumps(request))

        assert response.status_code == 200, response

        body = response.get_json()
        assert body['image'] == request['image'], body
        assert body['name'] == request['name'], body
        assert body['num_reviews'] == 0, body
        assert body['avg_rating'] == 0, body

        record = Restaurant.query.get(body['id'])
        assert record.name == 'KFC'
        assert record.image == 'image.jpg'

    def test_delete__not_logged_in(self):
        record_id = create_restaurant()

        response = self.client.delete('/restaurant/%s' % record_id)

        assert response.status_code == 401, response

    def test_delete__not_admin(self):
        record_id = create_restaurant()

        create_non_admin_account_and_login(self.client)

        response = self.client.delete('/restaurant/%s' % record_id)

        assert response.status_code == 403, response

    def test_delete__does_not_exist(self):
        create_admin_and_login(self.client)

        response = self.client.delete('/restaurant/does_not_exist')

        assert response.status_code == 400, response

    def test_delete__good(self):
        record_id = create_restaurant()
        create_review(self.author.id, record_id)
        create_review(self.author.id, record_id)
        create_review(self.author.id, record_id)

        create_admin_and_login(self.client)

        response = self.client.delete('/restaurant/%s' % record_id)

        assert response.status_code == 200, response

        # Check that the record was deleted from the database
        assert Restaurant.query.get(record_id) is None

        # Check that the restaurant causes a cascade delete of the restaurant's reviews
        reviews = Review.query.filter_by(restaurant_id=record_id).all()
        assert len(reviews) == 0

    def test_list_good(self):
        create_restaurant('KFC', 1.0)
        create_restaurant('Ruth Chris Steakhouse', 5.0)
        create_restaurant('Mikes Cereal Shack', 3.0)

        response = self.client.get('/restaurant')

        assert response.status_code == 200, response

        body = response.get_json()
        assert len(body) == 3, body
        assert body[0]['name'] == 'Ruth Chris Steakhouse', body[0]
        assert body[1]['name'] == 'Mikes Cereal Shack', body[1]
        assert body[2]['name'] == 'KFC', body[2]

        # Check that the reviews aren't being pulled for the list
        print(body)
        assert 'reviews' not in body[0]
        assert 'reviews' not in body[1]
        assert 'reviews' not in body[2]

    def test_read__does_not_exist(self):
        response = self.client.get('/restaurant/does_not_exist')

        assert response.status_code == 400, response
        assert response.get_json() == {'message': 'no matching record'}, response.get_json()

    def test_read__good(self):
        restaurant_id = create_restaurant('KFC', 1.0)
        restaurant = Restaurant.query.get(restaurant_id)

        # Setup some reviews to be included in the read
        create_review(self.author.id, restaurant_id, 3.0)
        review2 = create_review(self.author.id, restaurant_id, 5.0)
        review3 = create_review(self.author.id, restaurant_id, 2.0)
        restaurant.maintain_summary()
        db.session.commit()

        response = self.client.get('/restaurant/%s' % restaurant_id)

        assert response.status_code == 200, response

        body = json.loads(response.data)
        assert body['avg_rating'] == 3.3, body
        assert body['name'] == 'KFC', body
        assert body['num_reviews'] == 3, body
        assert body['best_review_id'] == review2.id, body
        assert body['worst_review_id'] == review3.id, body
        assert len(body['reviews']) == 3, body

        # Check that the review author names are joined
        assert body['reviews'][0]['author_name'] == 'Bob Villa', body
        assert body['reviews'][0]['author_name'] == 'Bob Villa', body
        assert body['reviews'][0]['author_name'] == 'Bob Villa', body

    def test_update__not_logged_in(self):
        record_id = create_restaurant()

        changes = {
            'name': 'Waffle House',
            'image': 'meow.jpg',
        }
        response = self.client.put('/restaurant/%s' % record_id, data=json.dumps(changes))

        assert response.status_code == 401, response

    def test_update__not_admin(self):
        record_id = create_restaurant()

        create_non_admin_account_and_login(self.client)

        changes = {
            'name': 'Waffle House',
            'image': 'meow.jpg',
        }
        response = self.client.put('/restaurant/%s' % record_id, data=json.dumps(changes))

        assert response.status_code == 403, response

    def test_update__does_not_exist(self):
        create_admin_and_login(self.client)

        changes = {
            'name': 'Waffle House',
            'image': 'meow.jpg',
        }
        response = self.client.put('/restaurant/not_here', data=json.dumps(changes))

        assert response.status_code == 400, response
        assert response.get_json() == {'message': 'no matching record'}, response.get_json()

    def test_update__bad_field_value(self):
        record_id = create_restaurant()

        create_admin_and_login(self.client)

        changes = {
            'name': 'W'*9000,
            'image': 'meow.jpg',
        }
        response = self.client.put('/restaurant/%s' % record_id, data=json.dumps(changes))

        assert response.status_code == 400, response.status_code
        assert response.get_json() == {
            'message': 'malformed fields',
            'payload': {
                'name': 'length must be <= 64, got 9000',
            },
        }, response.get_json()

    def test_update__good(self):
        record_id = create_restaurant()

        create_admin_and_login(self.client)

        changes = {
            'name': 'Waffle House',
            'image': 'meow.jpg',
        }
        response = self.client.put('/restaurant/%s' % record_id, data=json.dumps(changes))

        assert response.status_code == 200, response.status_code

        # Check that the record was properly updated
        record = Restaurant.query.get(record_id)
        assert record.name == 'Waffle House'
        assert record.image == 'meow.jpg'
