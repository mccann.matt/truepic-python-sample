import flask
import flask_login

from sqlalchemy.exc import IntegrityError

from topfood.api.api_root import api, db
from topfood.db.models.account import Account
from topfood.helpers.auth_helper import admin_only
from topfood.helpers.crud_helper import delete_record, list_records, update_record
from topfood.helpers.response_helper import build_bad_request_response, build_conflict_response, build_ok_response


@api.route('/account', methods=['POST'])
def account_create():
    """ Creates a new user account record """
    request = flask.request.json

    account = Account()
    did_apply_fail, error_response = account.apply_create_request_fields(request)

    if did_apply_fail:
        return error_response

    # Check if an account is already registered with this email
    accounts_with_email = db.session.query(Account).filter_by(email=request['email']).all()
    if len(accounts_with_email) > 0:
        return build_conflict_response('duplicate email')

    # If this account is being created by an admin user, allow them to set the is_admin flag
    is_admin = hasattr(flask_login.current_user, 'is_admin') and flask_login.current_user.is_admin
    if is_admin and 'is_admin' in request:
        account.is_admin = request['is_admin']

    # Build the response before commit to avoid staleness re-read
    response = build_ok_response(account)

    db.session.add(account)
    db.session.commit()

    return response


@api.route('/account/<account_id>', methods=['DELETE'])
@admin_only
def account_delete(account_id):
    account = Account.query.get(account_id)
    if account is None:
        return build_bad_request_response('no matching record')

    # Delete the user's reviews, and recalculate the reviewed restaurant's summary details
    for review in account.reviews:
        db.session.delete(review)
        review.restaurant.maintain_summary()

    db.session.delete(account)
    db.session.commit()

    return build_ok_response()


@api.route('/account', methods=['GET'])
@admin_only
def account_list():
    order_by = Account.email

    return list_records(Account, order_by)


@api.route('/account/<account_id>', methods=['PUT'])
@admin_only
def account_update(account_id):
    request_data = flask.request.json

    try:
        return update_record(Account, account_id, request_data)
    # Catch the index integrity error thrown when updating the email to an already exist value
    except IntegrityError as ex:
        return build_conflict_response('duplicate email')
