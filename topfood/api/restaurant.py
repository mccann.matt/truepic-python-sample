import flask

from topfood.api.api_root import api, db
from topfood.db.models.restaurant import Restaurant
from topfood.helpers.auth_helper import admin_only
from topfood.helpers.crud_helper import create_record, delete_record, list_records, update_record
from topfood.helpers.response_helper import build_bad_request_response, build_ok_response


@api.route('/restaurant', methods=['POST'])
@admin_only
def restaurant_create():
    request = flask.request.json

    return create_record(Restaurant, request)


@api.route('/restaurant/<restaurant_id>', methods=['DELETE'])
@admin_only
def restaurant_delete(restaurant_id):
    return delete_record(Restaurant, restaurant_id)


@api.route('/restaurant', methods=['GET'])
def restaurant_list():
    order_by = Restaurant.avg_rating.desc()

    return list_records(Restaurant, order_by)


@api.route('/restaurant/<restaurant_id>', methods=['GET'])
def restaurant_read(restaurant_id):
    restaurant = Restaurant.query.get(restaurant_id)
    if restaurant is None:
        return build_bad_request_response('no matching record')

    for review in restaurant.reviews:
        review.author_name = review.author.name

    return build_ok_response(restaurant.to_json_friendly())


@api.route('/restaurant/<restaurant_id>', methods=['PUT'])
@admin_only
def restaurant_update(restaurant_id):
    request_data = flask.request.json

    return update_record(Restaurant, restaurant_id, request_data)
