import os
import tempfile

import flask_testing
from flask.testing import FlaskClient

from topfood.api.api_root import api, db
from topfood.db.models.account import Account
from topfood.db.models.restaurant import Restaurant
from topfood.db.models.review import Review


# Configure the default content type to application/json
class TestClient(FlaskClient):
    def open(self, *args, **kwargs):
        kwargs.setdefault('content_type', 'application/json')
        return super().open(*args, **kwargs)


class TestCase(flask_testing.TestCase):
    @staticmethod
    def create_account(email='bob@bob.com', name='Bob Villa'):
        account = Account()
        account.name = name
        account.email = email
        account.password = 'password'

        db.session.add(account)
        db.session.commit()

        return account

    @staticmethod
    def create_restaurant(name='KFC', image='image.jpg'):
        restaurant = Restaurant()
        restaurant.name = name
        restaurant.image = image

        db.session.add(restaurant)
        db.session.commit()

        return restaurant

    @staticmethod
    def create_review(author, restaurant, rating=3.0):
        review = Review()
        review.author = author
        review.restaurant = restaurant
        review.rating = rating
        review.visit_date = '2019-01-01'

        db.session.add(review)
        db.session.commit()

        return review

    def create_app(self):
        """ Called by flask_testing.TestCase before each test """
        api.testing = True
        api.test_client_class = TestClient

        return api

    @classmethod
    def setUpClass(cls):
        # Use a temp-file database
        cls.db_fd, api.config['DATABASE'] = tempfile.mkstemp()

    def setUp(self):
        # Initialize the database
        db.drop_all()
        db.create_all()

    @classmethod
    def tearDownClass(cls):
        # Clean up the temp file database
        os.close(cls.db_fd)
        os.unlink(api.config['DATABASE'])
