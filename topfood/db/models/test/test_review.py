from datetime import date

from topfood.api.api_root import db
from topfood.db.models.account import Account
from topfood.db.models.restaurant import Restaurant
from topfood.db.models.review import Review
from topfood.test.test_case import TestCase


class TestReview(TestCase):
    def setUp(self):
        super(TestReview, self).setUp()

        self.record = Review()

    def test_commit(self):
        author = Account()
        author.name = 'Bob Villa'
        author.email = 'bob@bob.com'
        author.password = 'password'

        restaurant = Restaurant()
        restaurant.name = 'KFC'
        restaurant.image = 'image.jpg'

        self.record.author = author
        self.record.restaurant = restaurant
        self.record.visit_date = '2018-12-12'

        db.session.add(self.record)
        db.session.commit()

    def test_comment__too_long(self):
        try:
            self.record.comment = 'A'*1001
            assert False, "Should have thrown"
        except ValueError:
            pass

    def test_comment__good(self):
        self.record.comment = 'Woof'

        assert self.record.comment == 'Woof'

    def test_rating__too_low(self):
        try:
            self.record.rating = 0.9
            assert False, "Should have thrown"
        except ValueError:
            pass

    def test_rating__too_high(self):
        try:
            self.record.rating = 5.1
            assert False, "Should have thrown"
        except ValueError:
            pass

    def test_rating__good(self):
        self.record.rating = 1.5

        assert self.record.rating == 1.5
