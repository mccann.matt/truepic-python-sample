from topfood.db.models.record import RecordMixin
from topfood.helpers.assert_helper import assert_min_value
from topfood.test.test_case import TestCase


# Used a test fixture for testing the RecordMixin
class SomeRecordClass(RecordMixin):
    CREATE_FIELDS_REQUIRED = ['foo']

    CREATE_FIELDS_OPTIONAL = ['bar']

    READABLE_FIELDS = ['foo']

    UPDATEABLE_FIELDS = ['bar']

    def __init__(self):
        self._bar = 100
        self._foo = 10

    @property
    def bar(self):
        return self._bar

    @bar.setter
    def bar(self, bar):
        assert_min_value(100, bar)

        self._bar = bar

    @property
    def foo(self):
        return self._foo

    @foo.setter
    def foo(self, foo):
        assert_min_value(10, foo)

        self._foo = foo


class TestRecordMixin(TestCase):
    def test_apply_create_request_fields__missing_required_fields(self):
        record = SomeRecordClass()
        request = {'bar': 1000}

        did_error_occur, error_response = record.apply_create_request_fields(request)

        assert did_error_occur is True
        assert error_response.status_code == 400
        assert error_response.get_json() == {
            'message': 'missing required fields',
            'payload': ['foo'],
        }

    def test_apply_create_request_fields__malformed_fields(self):
        record = SomeRecordClass()
        request = {'foo': 9, 'bar': 1000}

        did_error_occur, error_response = record.apply_create_request_fields(request)

        assert did_error_occur is True
        assert error_response.status_code == 400
        assert error_response.get_json() == {
            'message': 'malformed fields',
            'payload': {
                'foo': 'must be >= 10, got 9',
            },
        }, error_response.get_json()

    def test_apply_create_request_fields__good(self):
        record = SomeRecordClass()
        request = {'foo': 11, 'bar': 1001, 'notme': 11}

        did_error_occur, error_response = record.apply_create_request_fields(request)

        assert did_error_occur is False
        assert error_response is None
        assert record.foo == 11
        assert record.bar == 1001

    def test_update_request_fields__malformed_fields(self):
        record = SomeRecordClass()
        request = {'foo': 9, 'bar': 1}

        did_error_occur, error_response = record.apply_update_request_fields(request)

        assert did_error_occur is True
        assert error_response.status_code == 400
        assert error_response.get_json() == {
            'message': 'malformed fields',
            'payload': {
                'bar': 'must be >= 100, got 1',
            },
        }, error_response.get_json()

    def test_update_request_fields__good(self):
        record = SomeRecordClass()
        request = {'foo': 11, 'bar': 101, 'notme': 11}

        did_error_occur, error_response = record.apply_update_request_fields(request)

        assert did_error_occur is False
        assert error_response is None
        assert record.foo == 10
        assert record.bar == 101

    def test_to_json_friendly(self):
        record = SomeRecordClass()

        assert record.to_json_friendly() == {'foo': 10}, record.to_json_friendly()
