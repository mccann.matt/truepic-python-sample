from datetime import date

from topfood.api.api_root import db
from topfood.db.models.account import Account
from topfood.db.models.restaurant import Restaurant
from topfood.db.models.review import Review
from topfood.test.test_case import TestCase


class TestRestaurant(TestCase):
    def create_review(self, rating):
        review = Review()
        review.author = self.account
        review.restaurant = self.record
        review.rating = rating
        review.visit_date = '2012-12-12'

        db.session.add(review)

        return review

    def setUp(self):
        super(TestRestaurant, self).setUp()

        # Setup a test account
        self.account = Account()
        self.account.name = 'Bob Villa'
        self.account.email = 'bob@bob.com'
        self.account.password = 'password'

        # Set up an restaurant object to work with
        self.record = Restaurant()
        self.record.name = 'KFC'
        self.record.image = 'image.jpg'

    def test_commit(self):
        db.session.add(self.record)
        db.session.commit()

    def test_construct(self):
        record = Restaurant()

        assert record.id is not None, record
        assert record.avg_rating == 0, record
        assert record.best_review_id is None, record
        assert record.num_reviews == 0, record
        assert record.worst_review_id is None, record

    def test_avg_rating__fixed_to_one_digit(self):
        self.record._avg_rating = 45

        assert self.record.avg_rating == 4.5, self.record.avg_rating

    def test_image__empty(self):
        try:
            self.record.image = ''
            assert False, "Should have thrown ValueError"
        except ValueError:
            pass  # Threw error as expected

    def test_image__too_long(self):
        try:
            self.record.image = 'A' * 129
            assert False, "Should have thrown ValueError"
        except ValueError:
            pass  # Threw error as expected

    def test_image__good(self):
        image = 'google.com/image.jpg'

        self.image = image
        assert self.image == image, self.image

    def test_maintain_summary__no_reviews(self):
        db.session.add(self.record)
        db.session.commit()

        self.record.maintain_summary()

        assert self.record.num_reviews == 0
        assert self.record.avg_rating == 0
        assert self.record.best_review_id is None
        assert self.record.worst_review_id is None

    def test_maintain_summary__one_review(self):
        db.session.add(self.record)
        review = self.create_review(3.0)
        db.session.commit()

        self.record.maintain_summary()

        assert self.record.num_reviews == 1
        assert self.record.avg_rating == 3.0
        assert self.record.best_review_id == review.id
        assert self.record.worst_review_id == review.id

    def test_maintain_summary__several_reviews(self):
        db.session.add(self.record)
        db.session.flush()
        self.create_review(3.0)
        review2 = self.create_review(5.0)
        review3 = self.create_review(2.0)
        db.session.commit()

        self.record.maintain_summary()

        assert self.record.num_reviews == 3
        assert self.record.avg_rating == 3.3, self.record.avg_rating
        assert self.record.best_review_id == review2.id
        assert self.record.worst_review_id == review3.id

    def test_name__empty(self):
        try:
            self.record.name = ''
            assert False, "Should have thrown ValueError"
        except ValueError:
            pass  # Threw error as expected

    def test_name__too_long(self):
        try:
            self.record.name = 'A' * 65
            assert False, "Should have thrown ValueError"
        except ValueError:
            pass  # Threw error as expected

    def test_name__good(self):
        name = 'KFC'

        self.record.name = name
        assert self.record.name == name, self.record.name
