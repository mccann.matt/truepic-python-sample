from topfood.api.api_root import db
from topfood.db.models.account import Account
from topfood.test.test_case import TestCase


class TestAccount(TestCase):
    def setUp(self):
        super(TestAccount, self).setUp()

        # Set up an account object to work with
        self.account = Account()
        self.account.name = 'Bob Villa'
        self.account.email = 'bob@bob.com'
        self.account.password = 'password'

    def test_commit(self):
        db.session.add(self.account)
        db.session.commit()

    def test_construct(self):
        record = Account()

        assert record.id is not None
        assert record.is_admin is False
        assert record._password_salt is not None

    def test_email_set_too_long(self):
        try:
            self.account.email = 'A' * 65 + '@gmail.com'
            assert False, "Should have thrown ValueError"
        except ValueError:
            pass  # Threw error as expected

    def test_email_set_empty(self):
        try:
            self.account.email = ''
            assert False, "Should have thrown ValueError"
        except ValueError:
            pass  # Threw error as expected

    def test_email_set_malformed(self):
        try:
            self.account.email = 'snicklefritz'
            assert False, "Should have thrown ValueError"
        except ValueError:
            pass  # Threw error as expected

    def test_email_set_good(self):
        self.account.email = "mccann.matt@gmail.com"

        # Did not throw a ValueError!

    def test_name_set_too_long(self):
        try:
            self.account.name = 'A' * 65
            assert False, "Should have thrown ValueError"
        except ValueError:
            pass  # Threw error as expected

    def test_name_empty(self):
        try:
            self.account.name = ''
            assert False, "Should have thrown ValueError"
        except ValueError:
            pass  # Threw error as expected

    def test_name_good(self):
        self.account.name = 'Bob'

        # Did not throw a ValueError!

    def test_is_matching_password_matching_password(self):
        assert self.account.is_matching_password('password') is True

    def test_is_matching_password_not_matching_password(self):
        """ Tests checking if a not matching password does in fact match. """
        assert self.account.is_matching_password('notpassword') is False

    def test_password_set_too_short(self):
        try:
            self.account.password = 'A' * 4
            assert False, "Should have thrown ValueError"
        except ValueError:
            pass  # Threw error as expected

    def test_password_good_value(self):
        self.password = 'Password123'

        # Did not throw a ValueError!
