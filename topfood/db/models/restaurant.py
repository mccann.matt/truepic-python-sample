from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship
from uuid import uuid4

import topfood.helpers.assert_helper as asserts

from topfood.api.api_root import db
from topfood.db.models.record import RecordMixin


class Restaurant(db.Model, RecordMixin):
    CREATE_FIELDS_REQUIRED = ['image', 'name']

    LISTABLE_FIELDS_BLACKLIST = ['best_review_id', 'reviews', 'worst_review_id']

    READABLE_FIELDS_BLACKLIST = []  # All fields are readable

    UPDATEABLE_FIELDS = ['image', 'name']

    __tablename__ = 'Restaurant'

    # Accumulated and maintained by the maintain_summary function
    # Average rating is stored as avg_rating * 10, as to keep a fixed length value
    _avg_rating = Column('avg_rating', Integer(), nullable=False)

    # Accumulated and maintained by the maintain_summary function
    _best_review_id = Column('best_review_id', String(length=36), nullable=True)

    _id = Column('id', String(length=36), primary_key=True)

    _image = Column('image', String(length=128), nullable=False)

    _name = Column('name', String(length=64), nullable=False)

    # Accumulated and maintained by the maintain_summary function
    _num_reviews = Column('num_reviews', Integer(), nullable=False)

    reviews = relationship(
        "Review", back_populates="restaurant", cascade='save-update, delete', order_by='desc(Review.visit_date)',
    )

    # Accumulated and maintained by the maintain_summary function
    _worst_review_id = Column('worst_review_id', String(length=36), nullable=True)

    def __init__(self):
        self._avg_rating = 0
        self._id = str(uuid4())
        self._num_reviews = 0

    def __repr__(self):
        return '<Restaurant(id=%s, name=%s)>' % (self.id, self.name)

    @hybrid_property
    def avg_rating(self):
        return self._avg_rating / 10.0

    @hybrid_property
    def best_review_id(self):
        return self._best_review_id

    @hybrid_property
    def id(self):
        return self._id

    @hybrid_property
    def image(self):
        return self._image

    @image.setter
    def image(self, image):
        asserts.assert_not_empty_string(image)
        asserts.assert_max_length_string(128, image)

        self._image = image

    # Update the summary details for the restaurant being reviewed - write cost is high, but keeps read cost low
    def maintain_summary(self):
        self._num_reviews = len(self.reviews)
        self._avg_rating = 0
        self._best_review_id = None
        self._worst_review_id = None

        if len(self.reviews):
            self._avg_rating = int(sum(review.rating for review in self.reviews) / self.num_reviews * 10)
            self._best_review_id = max(self.reviews, key=lambda review: review.rating).id
            self._worst_review_id = min(self.reviews, key=lambda review: review.rating).id

    @hybrid_property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        asserts.assert_not_empty_string(name)
        asserts.assert_max_length_string(64, name)

        self._name = name

    @hybrid_property
    def num_reviews(self):
        return self._num_reviews

    @hybrid_property
    def worst_review_id(self):
        return self._worst_review_id
