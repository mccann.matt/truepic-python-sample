import hashlib

from flask_login import UserMixin
from sqlalchemy import Boolean, Column, String
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship
from uuid import uuid4

import topfood.helpers.assert_helper as asserts

from topfood.api.api_root import db
from topfood.db.models.record import RecordMixin


class Account(UserMixin, db.Model, RecordMixin):
    CREATE_FIELDS_REQUIRED = ['email', 'name', 'password']

    READABLE_FIELDS = ['email', 'id', 'is_admin', 'name']

    UPDATEABLE_FIELDS = ['email', 'is_admin', 'name']

    __tablename__ = 'Account'

    _email = Column('email', String(length=64), index=True, unique=True)

    _id = Column('id', String(length=36), primary_key=True)

    is_admin = Column(Boolean(), nullable=False)

    _name = Column('name', String(length=25), nullable=False)

    """ sha256 hash of user password + salt """
    _password_hash = Column('password_hash', String(length=300), nullable=False)

    """ Randomly generated value added to the user password before hashing """
    _password_salt = Column('password_salt', String(length=36), nullable=False)

    reviews = relationship("Review")

    def __init__(self):
        self._id = str(uuid4())
        self.is_admin = False
        self._password_salt = str(uuid4())

    def __repr__(self):
        return '<Account(id=%s, email=%s)>' % (self.id, self.email)

    @hybrid_property
    def email(self):
        return self._email

    @email.setter
    def email(self, email):
        asserts.assert_not_empty_string(email)
        asserts.assert_max_length_string(64, email)
        asserts.assert_valid_email(email)

        self._email = email

    @hybrid_property
    def id(self):
        return self._id

    def is_matching_password(self, password):
        """
        Salts and hashes the provided password to see if it matches the set password.

        @param password Password to compare
        @returns True if the password matches
        """
        return self._password_hash == str(hashlib.sha512((password + self._password_salt).encode('utf-8')).hexdigest())

    @hybrid_property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        asserts.assert_not_empty_string(name)
        asserts.assert_max_length_string(64, name)

        self._name = name

    # This is only here to satisfy the expectations of Flask's hybrid property, a decorator that is needed
    # to trigger a state change in the Flask record when setting _password_hash in the setter below
    @hybrid_property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        asserts.assert_not_empty_string(password)
        asserts.assert_min_length_string(8, password)

        # Generate the password hash
        self._password_hash = str(hashlib.sha512((password + self._password_salt).encode('utf-8')).hexdigest())

