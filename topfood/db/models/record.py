from sqlalchemy.orm.collections import InstrumentedList

from topfood.helpers.response_helper import build_bad_request_response


def gettable_fields_not_in_blacklist(fields, blacklist):
    """ Returns a list of fields that are gettable and not blacklisted """
    return filter(lambda x: x not in blacklist, fields)


def gettable_fields(record):
    """ Steps through all properties of the provide record and builds a list of the fields that have getters """
    fields = []

    for raw_key in record.__dict__.keys():
        # Pop off the "private" attribute leading underscore
        key = raw_key if not raw_key.startswith('_') else raw_key[1:]

        # If this is a private-only field (just leading underscore form available)
        if not hasattr(record, key):
            continue

        fields.append(key)

    return fields


def list_to_json_friendly(list_value):
    """ Packs the provided list into a JSON friendly representation """
    return [item.to_json_friendly() if hasattr(item, 'to_json_friendly') else item for item in list_value]


def safe_setattr(record, field, value, malformed_fields):
    """
    Wraps set_attr and catches any ValueErrors thrown by the field's setter when it
    validates the value.

    Adds the field and its error description to the map of malformed_fields if an error occurred
    """
    try:
        setattr(record, field, value)
    except ValueError as ex:
        malformed_fields[field] = str(ex)


class RecordMixin(object):
    # Override this in implementation to define fields that are optional as part of record creation
    CREATE_FIELDS_OPTIONAL = []

    # Override this in implementation to define fields that are required as part of record creation
    CREATE_FIELDS_REQUIRED = []

    # Override this in implementation to define fields that are visible when listing the records
    LISTABLE_FIELDS = []

    # Override this in implementation to define fields that are not visible when listing the records
    LISTABLE_FIELDS_BLACKLIST = None

    # Override this in implementation to define fields that are readable
    READABLE_FIELDS = []

    # Override this in implementation to define fields that are not readable
    READABLE_FIELDS_BLACKLIST = None

    # Override this in implementation to define fields are updateable
    UPDATEABLE_FIELDS = []

    def apply_create_request_fields(self, request):
        """
        Applies any fields in the request, as specified in CREATE_FIELDS_REQUIRED and CREATE_FIELDS_OPTIONAL,
        to the record.

        @returns (Did Error Occur, Response) - If error, response is the request response to return
        """
        malformed_fields = {}
        missing_fields = []

        # Apply the required fields
        for required_field in self.CREATE_FIELDS_REQUIRED:
            if required_field not in request:
                missing_fields.append(required_field)
            else:
                safe_setattr(self, required_field, request[required_field], malformed_fields)

        # If there are missing required fields
        if len(missing_fields) > 0:
            return True, build_bad_request_response('missing required fields', missing_fields)

        # Apply the optional fields
        for optional_field in self.CREATE_FIELDS_OPTIONAL:
            if optional_field in request:
                safe_setattr(self, optional_field, request[optional_field], malformed_fields)

        # If there are malformed fields
        if len(malformed_fields) > 0:
            return True, build_bad_request_response('malformed fields', malformed_fields)

        # Indicate that no error was detected
        return False, None

    def apply_update_request_fields(self, request):
        """
        Applies any fields in the request, as specified in UPDATEABLE_FIELDS, to the record.

        @returns (Did Error occur, Response) - If error, response is the request response to return
        """
        malformed_fields = {}

        for field in self.UPDATEABLE_FIELDS:
            if field in request:
                safe_setattr(self, field, request[field], malformed_fields)

        # If there are malformed fields
        if len(malformed_fields) > 0:
            return True, build_bad_request_response('malformed fields', malformed_fields)

        # Indicate that no error was detected
        return False, None

    def to_json_friendly(self, is_list_view=False):
        """ Builds a JSON compatible representation of the record for returning in response data """
        fields_to_pack = self.LISTABLE_FIELDS if is_list_view else self.READABLE_FIELDS
        friendly_repr = {}

        # If a blacklist is defined, use that to generate the list of packable fields
        fields_blacklist = self.LISTABLE_FIELDS_BLACKLIST if is_list_view else self.READABLE_FIELDS_BLACKLIST
        if fields_blacklist is not None:
            fields_to_pack = gettable_fields_not_in_blacklist(gettable_fields(self), fields_blacklist)

        # Pack the fields included in the representation
        for key in fields_to_pack:
            if hasattr(self, key):
                field = getattr(self, key)

                if type(field) in [list, InstrumentedList]:
                    friendly_repr[key] = list_to_json_friendly(field)
                elif hasattr(field, 'to_json_friendly'):
                    friendly_repr[key] = field.to_json_friendly()
                else:
                    friendly_repr[key] = field

        return friendly_repr
