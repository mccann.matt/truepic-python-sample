from datetime import datetime
from sqlalchemy import Column, Date, ForeignKey, Integer, String
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship
from uuid import uuid4

import topfood.helpers.assert_helper as asserts

from topfood.api.api_root import db
from topfood.db.models import DATE_FORMAT
from topfood.db.models.record import RecordMixin


class Review(db.Model, RecordMixin):
    CREATE_FIELDS_REQUIRED = ['rating', 'restaurant_id', 'visit_date']

    CREATE_FIELDS_OPTIONAL = ['comment']

    READABLE_FIELDS = ['author_id', 'author_name', 'comment', 'id', 'rating', 'visit_date']

    UPDATEABLE_FIELDS = ['comment', 'rating', 'visit_date']

    __tablename__ = 'Review'

    author = relationship("Account", back_populates="reviews")
    author_id = Column(String(length=36), ForeignKey('Account.id'), nullable=False, index=True)

    _comment = Column('comment', String(length=1000), nullable=False)

    id = Column(String(length=36), primary_key=True)

    _rating = Column('rating', Integer())

    restaurant_id = Column(String(length=36), ForeignKey('Restaurant.id'), nullable=False, index=True)
    restaurant = relationship("Restaurant", back_populates="reviews", foreign_keys=[restaurant_id])

    _visit_date = Column('visit_date', String(length=len(DATE_FORMAT)), nullable=False)

    def __init__(self):
        self.id = str(uuid4())
        self.comment = ''

    def __repr__(self):
        return '<Review(id=%s)>' % self.id

    @hybrid_property
    def comment(self):
        return self._comment

    @comment.setter
    def comment(self, comment):
        asserts.assert_max_length_string(1000, comment)

        self._comment = comment

    @hybrid_property
    def rating(self):
        return self._rating

    @rating.setter
    def rating(self, rating):
        asserts.assert_min_value(1, rating)
        asserts.assert_max_value(5, rating)

        self._rating = rating

    @hybrid_property
    def visit_date(self):
        return self._visit_date

    @visit_date.setter
    def visit_date(self, visit_date):
        try:
            # Check the format
            datetime.strptime(visit_date, DATE_FORMAT).date()

            self._visit_date = visit_date
        except ValueError:
            raise ValueError('format must be %s, got %s' % (DATE_FORMAT, visit_date))
