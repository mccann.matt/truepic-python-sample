'''
@brief Configuration settings for the database connection
'''


class ProductionConfig(object):
    SQLALCHEMY_DATABASE_URI = 'TODO'


class TestConfig(object):
    SQLALCHEMY_DATABASE_URI = 'sqlite:////topfood/.topfood.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
