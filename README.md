# The TopFood API

You may optional use Docker to replicate the development environment:
```
./docker/build.py
./docker/launch_dev.sh
```

To bootstrap a local sqlite database for testing:
```
./bin/bootstrap_db.py
```

To stand up a local deployment of the API for testing:
```
./bin/run_api.py
```

To run the unit tests:
```
python3 -m nose
```
