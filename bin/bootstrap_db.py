#!/usr/bin/env python3

import sys
sys.path.insert(0, '.')

import os

os.environ['TARGET'] = 'test'

from topfood.api.api_root import db
from topfood.db.models.account import Account

db.create_all()

account = Account()
account.email = 'me@mattmccann.xyz'
account.password = 'password'
account.name = 'Matt McCann'
account.is_admin = True
db.session.add(account)
db.session.commit()
