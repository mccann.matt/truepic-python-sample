#!/usr/bin/env python3

import requests


base_url = 'http://localhost:8001'
session = requests.Session()


def post(url, request_data):
    print("POST %s/%s %s" % (base_url, url, request_data))
    r = session.post('%s/%s' % (base_url, url), json=request_data)
    print("%s %s" % (r.status_code, r.text))

    return r.json()


# Log into the default admin account
post("auth/login", {"email": "me@mattmccann.xyz", "password": "password"})

# Create a few user records
user = {"email": "bbarker@gmail.com", "password": "password", "name": "Bob Barker", "is_admin": False}
post("account", user)
user = {"email": "bob@thisoldehome.org", "password": "password", "name": "Bob Villa", "is_admin": True}
post("account", user)
user = {"email": "mike@google.com", "password": "password", "name": "Michael Scott", "is_admin": False}
post("account", user)
user = {"email": "tibitts@google.com", "password": "password", "name": "Mike Tibitts", "is_admin": False}
post("account", user)

# Create a few restaurants
restaurants = []
data = {"name": "KFC", "image": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQviOwkeEqmvGpSOhIPTqvUncHSHUjgCLfgJHv2W1T6gk-gH9KtDlgZ-Mo"}
restaurants.append(post("restaurant", data)["id"])
data = {"name": "Ruth Chris Steakhouse", "image": "https://cdn.ruthschris.com/~/media/images/menu/dinner/filet-steak.jpg"}
restaurants.append(post("restaurant", data)["id"])
data = {"name": "Bob Evans", "image": "https://tinyurl.com/ybazj58q"}
restaurants.append(post("restaurant", data)["id"])
data = {"name": "Shake Shack", "image": "https://tinyurl.com/yan7fuzq"}
restaurants.append(post("restaurant", data)["id"])
data = {"name": "McDonalds", "image": "https://tinyurl.com/ybda6ued"}
restaurants.append(post("restaurant", data)["id"])

users = ["bbarker@gmail.com", "bob@thisoldehome.org", "mike@google.com", "tibitts@google.com"]
for i in range(len(users)):
    post("auth/login", {"email": users[i], "password": "password"})

    for restaurant in restaurants:
        review = {
            "comment": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi eu sem massa. Nunc semper, libero nec posuere semper, neque felis euismod sem, quis auctor leo sem et felis. Nunc ultricies eros eget hendrerit faucibus. In hac habitasse platea dictumst. Sed et massa tincidunt, cursus nisi tempus, aliquam nunc. Integer fermentum elit at rutrum tincidunt. Ut feugiat ultricies dolor, ut molestie velit tempor non. Nam blandit egestas odio, sit amet fringilla nisl dapibus ac. Nullam id mattis dui. Nulla varius, purus in sagittis sagittis, erat justo interdum arcu, quis lobortis ligula augue id nulla. Ut cursus scelerisque commodo. Maecenas vel quam non tortor consequat aliquam id id lectus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse potenti.",
            "rating": i,
            "visit_date": "2019-01-%02d" % (i+1),
            "restaurant_id": restaurant,
        }
        post("review", review)
